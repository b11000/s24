// Query Operators
// 
// To achieve that kind of query, we need to use the $gt(greater than) and $lt (less than), $lte(less than or equal), $ne (not equal), $in(), $or(), $and()
// ,i(case insensitive)
// examples:
// db.courses.find({ price: {$gt: 1000 }})
// 
// db.users.find({ age: {$gt: 50}})
// 
// db.users.find({ lastName: {$in: "Hawkins", "Doe"}})
// 
// db.users.find({ lastName: {$in: ["Hawking", "Doe"]}})
// 
// db.users.find({ courses: {$in: ["Sass", "React"]}})
// 
// db.users.find({ $or: [{ firstName: "Neil"}, {age: 21}] })
// 
// db.users.find({ $and: [{age: 21},{age: {$ne: 76}}] })
// 
// REGEX
// for filter
// Can search strings
// db.courses.find({ name: { $regex: '0' } });
// 
// db.users.find( { firstName: { $regex: 'N' } } )
// 
// CASE INSENSITIVE
// db.users.find( { firstName: { $regex: 'j', $options: '$i' } } )
// 
// conbined into a single query like this.
// 
// 
// Including or Excluding Fields
// 
// db.usersi.find({}, { "_id": 0});
// 
// db.users.find( { firstName: "John" },
// {
//     "_id": 0,
//     firstName: 1,
//     lastName: 1
//     }
// )

// db.users.find( { firstName: "John" },
// {
//     "_id": 0,
//     firstName: 1,
//     lastName: 1,
//     contact: { phone: 1}
//     }
// )
// 
// db.users.find( { firstName: "John" },
// {
//     "_id": 0,
//     firstName: 1,
//     lastName: 1,
//     "contact.phone": 1
//     }
// )
// 
// db.users.find( { firstName: "John" },
// {
//     "_id": 0,
//     courses: { $slice: [1, 2] }
//     })
// 
// // $gt/$gte operator
		/*
			- Allows us to find documents that have field number values greater than or equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $gt : value } });
				db.collectionName.find({ field : { $gte : value } });
		*/

		// $lt/$lte operator
		/*
			- Allows us to find documents that have field number values less than or equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $lt : value } });
				db.collectionName.find({ field : { $lte : value } });
		*/

		// $ne operator
		/*
			- Allows us to find documents that have field number values not equal to a specified value.
			- Syntax
				db.collectionName.find({ field : { $ne : value } });
		*/

		// $in operator
		/*
			- Allows us to find documents with specific match critieria one field using different values.
			- Syntax
				db.collectionName.find({ field : { $in : value } });
		*/

		// Logical Query Operators

		// $or operator
		/*
			- Allows us to find documents that match a single criteria from multiple provided search criteria.
			- Syntax
				db.collectionName.find({ $or: [ { fieldA: valueB }, { fieldB: valueB } ] });
		*/
		// $and operator
		/*
			- Allows us to find documents matching multiple criteria in a single field.
			- Syntax
				db.collectionName.find({ $and: [ { fieldA: valueB }, { fieldB: valueB } ] });
		*/

      // $regex operator
		/*
			- Allows us to find documents that match a specific string pattern using regular expressions.
			- Syntax
				db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
		*/

		// Case sensitive query
		db.users.find({ firstName: { $regex: 'N' } })

		db.users.find({ firstName: { $regex: 'j' } }).
		//There will be no result in the given query since the search is case sensitive.
		//to make the search case insensitive, let’s add one more operator named $options

		// Case insensitive query
		db.users.find({ firstName: { $regex: 'j', $options: '$i' } })
		//Here $options with ‘i’ parameter specifies that we want to carry out search without considering upper or lower case. Same will happen with our example in which “j” will be searched whether it is upper or lower case.


		// Field Projection
		/*
		//Sometimes, you do not want to retrieve all the properties of a document.
			- Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
			- When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
			- To help with readability of the values returned, we can include/exclude fields from the response.
		*/

		// Inclusion
		/*
			- Allows us to include/add specific fields only when retrieving documents.
			- The value provided is 1 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
		*/

		// Exclusion
		/*
			- Allows us to exclude/remove specific fields only when retrieving documents.
			- The value provided is 0 to denote that the field is being included.
			- Syntax
				db.users.find({criteria},{field: 1})
		*/


		// Suppressing the ID field
		/*
			- Allows us to exclude the "_id" field when retrieving documents.
			- When using field projection, field inclusion and exclusion may not be used at the same time.
			- Excluding the "_id" field is the only exception to this rule.
			- Syntax
				db.users.find({criteria},{_id: 0})
		*/

		// Returning Specific Fields in Embedded Documents
		db.users.find( 
			{ 
				firstName: "John" 
			}, 
			{ 
				firstName: 1, 
				lastName: 1,
				"contact.phone": 1
			}
		)

		// Supressing Specific Fields in embedded documents
		db.users.find( 
			{ 
				firstName: "John" 
			}, 
			{ 
				"contact.phone": 0
			}
		)